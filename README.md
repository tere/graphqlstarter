# Simple GraphQL Server
stripped down version of [this][3]

es6 syntax. don't get scared

## With
 - nodejs
 - express
 - mysql
 - graphql


## Requirements
 - NodeJS (`v6.10`)
 - [babel-cli][1]
 - mysql server (xammp works too) 


## Setup
- Run `npm install` in root folder
- Create database
- in db.js setup connection config according to comments ^^
- open terminal & run `babel-node server.js` -> go to [localhost:3000][2]





[1]:https://babeljs.io/docs/usage/cli/
[2]:localhost:3000
[3]:https://www.youtube.com/watch?v=DNPVqK_woRQ