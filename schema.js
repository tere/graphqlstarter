import {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull,
	GraphQLText
} from 'graphql';
import Db from './db.js';



const Picture = new GraphQLObjectType({
	name: 'Picture',
	description: 'This represents an Picture',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(picture) {
					return picture.id;
				}
			},
			title: {
				type: GraphQLString,
				resolve(picture) {
					return picture.title;
				}
			},
			description: {
				type: GraphQLString,
				resolve(picture) {
					return picture.description;
				}
			},
			tags: {
				type: GraphQLString,
				resolve(picture) {
					return picture.tags;
				}
			},
		};
	}
});

const User = new GraphQLObjectType({
	name: 'User',
	description: 'This represents an User',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(user) {
					return user.id;
				}
			},
			username: {
				type: GraphQLString,
				resolve(user) {
					return user.username;
				}
			},
			firstName: {
				type: GraphQLString,
				resolve(user) {
					return user.firstName;
				}
			},
			lastName: {
				type: GraphQLString,
				resolve(user) {
					return user.lastName;
				}
			},
			email: {
				type: GraphQLString,
				resolve(user) {
					return user.email;
				}
			},
			pictures: {
				type: Picture,
				resolve(user) {
					return user.getPictures();
				}
			},
		}
	}
});

const Image = new GraphQLObjectType({
	name: 'Image',
	description: 'This represents an Image',
	fields: () => {
		return {
			id: {
				type: GraphQLInt,
				resolve(image) {
					return image.id;
				}
			},
			path: {
				type: GraphQLString,
				resolve(image) {
					return image.path;
				}
			}
		}
	}
});


const Query = new GraphQLObjectType({
	name: 'Query',
	description: 'This is a root query',
	fields: () => {
		return {
			users: {
				type: new GraphQLList(User),
				args: {
					id: {
						type: GraphQLInt
					},
					username: {
						type: GraphQLString
					},
					email: {
						type: GraphQLString
					}
				},
				resolve(root, args) {
					return Db.models.user.findAll({where: args});
				}
			},
			pictures: {
				type: new GraphQLList(Picture),
				args: {
					id: {
						type: GraphQLInt,
					},
					tags: {
						type: GraphQLString,
					}
				},
				resolve(root, args) {
					return Db.models.picture.findAll({where: args});
				}
			},
			images: {
				type: new GraphQLList(Image),
				args: {
					id: {
						type: GraphQLInt
					}
				},
				resolve(root, args) {
					return Db.models.image.findAll({where: args});
				}
			}
		};
	}
});

// mutations are basically data manipulators ^^
const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	description:' Functions to create stuff',
	fields() {
		return {
			addUser: {
				type: User,
				args: {
					username: {
						type: new GraphQLNonNull(GraphQLString),
					},
					password: {
						type: new GraphQLNonNull(GraphQLString),
					},
					firstName: {
						type: GraphQLString,
					},
					lastName: {
						type: GraphQLString,
					},
					email: {
						type: GraphQLString,
					}
				},
				resolve(_, args) {
					return Db.models.user.create({
						username: args.username,
						password: args.password,
						firstName: args.firstName,
						lastName: args.lastName,
						email: args.email,
					})
				}
			},
			addImage: {
				type: Image,
				args: {
					path: {
						type: new GraphQLNonNull(GraphQLString),
					}
				},
				resolve(_, args) {
					return Db.models.image.create({
						path: args.path
					});
				}
			},
			addPicture: {
				
			}
		};
	}
});

const Schema = new GraphQLSchema({
	query: Query,
	mutation: Mutation
});

export default Schema;