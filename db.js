const Sequelize =  require('sequelize');

const Conn = new Sequelize(
	'graphqltest', //database name
	'root', //username
	'', //password
	{
		dialect: 'mysql', //database type
		host: 'localhost' //well...host ^^
	}
);

const User = Conn.define('user', {
	username: {
		type: Sequelize.STRING,
		allowNull: false,
		validate: {
			is: /^[a-z0-9]+$/i
		}
	},
	password: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	firstName: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	lastName: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	email: {
		type: Sequelize.STRING,
		allowNull: false,
		validate: {
			isEmail: true,
		}
	}
});

const Picture = Conn.define('picture', {
	title: {
		type: Sequelize.STRING,
	},
	description: {
		type: Sequelize.STRING,
	},
	tags: {
		type: Sequelize.STRING
	}
});

const Image = Conn.define('image', {
	path: {
		type: Sequelize.STRING,
	},
	meta: {
		type: Sequelize.TEXT
	}
});

User.hasMany(Picture);
Picture.belongsTo(User);
Picture.hasOne(Image);
Image.belongsTo(Picture);


Conn.sync({force: true}).then(() => {
	console.log(`Synced\n`);
});

export default Conn;